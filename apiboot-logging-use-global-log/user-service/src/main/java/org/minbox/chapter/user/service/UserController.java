package org.minbox.chapter.user.service;

import org.minbox.framework.logging.client.global.GlobalLogging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试用户控制器
 *
 * @author 恒宇少年
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {
    /**
     * {@link GlobalLogging}
     *
     * @see org.minbox.framework.logging.client.global.AbstractGlobalLogging
     * @see org.minbox.framework.logging.client.global.support.GlobalLoggingMemoryStorage
     */
    @Autowired
    private GlobalLogging logging;

    /**
     * 测试获取用户名
     *
     * @return
     */
    @GetMapping(value = "/name")
    public String getUserName() {
        logging.debug("这是一条debug级别的日志，发生时间：{}");
        logging.info("这是一条info级别的日志，发生时间：{}", System.currentTimeMillis());
        try {
            int a = 5 / 0;
        } catch (Exception e) {
            logging.error("出现了异常.", e);
        }
        return "用户名：恒宇少年";
    }
}
