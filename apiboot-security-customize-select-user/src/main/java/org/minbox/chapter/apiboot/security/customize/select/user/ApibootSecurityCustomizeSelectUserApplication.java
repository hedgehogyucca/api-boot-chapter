package org.minbox.chapter.apiboot.security.customize.select.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApibootSecurityCustomizeSelectUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApibootSecurityCustomizeSelectUserApplication.class, args);
    }

}
