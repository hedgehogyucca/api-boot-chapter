package org.minbox.chapter.eureka.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author 恒宇少年
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication {
    /**
     * logger instance
     */
    static Logger logger = LoggerFactory.getLogger(EurekaServerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(EurekaServerApplication.class, args);
        logger.info("{}服务启动成功.", "");
    }
}
