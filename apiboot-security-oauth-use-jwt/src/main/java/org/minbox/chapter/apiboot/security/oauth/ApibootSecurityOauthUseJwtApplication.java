package org.minbox.chapter.apiboot.security.oauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApibootSecurityOauthUseJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApibootSecurityOauthUseJwtApplication.class, args);
	}

}
