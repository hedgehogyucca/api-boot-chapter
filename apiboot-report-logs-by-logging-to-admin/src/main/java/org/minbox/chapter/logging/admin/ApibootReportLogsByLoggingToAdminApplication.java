package org.minbox.chapter.logging.admin;

import org.minbox.framework.logging.spring.context.annotation.admin.EnableLoggingAdmin;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ApiBoot Logging Admin入口类
 */
@SpringBootApplication
@EnableLoggingAdmin
public class ApibootReportLogsByLoggingToAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApibootReportLogsByLoggingToAdminApplication.class, args);
    }

}
