package org.minbox.chapter.apiboot.security.oauth.custom.certification.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ApiBoot整合Security & OAuth2完成自定义读取用户
 *
 * @author 恒宇少年
 */
@SpringBootApplication
public class ApibootSecurityOauthCustomCertificationUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApibootSecurityOauthCustomCertificationUserApplication.class, args);
    }

}
