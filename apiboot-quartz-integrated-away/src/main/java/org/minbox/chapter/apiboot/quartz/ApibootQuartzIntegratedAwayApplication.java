package org.minbox.chapter.apiboot.quartz;

import org.minbox.framework.api.boot.plugin.quartz.ApiBootQuartzService;
import org.minbox.framework.api.boot.plugin.quartz.wrapper.support.ApiBootOnceJobWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApibootQuartzIntegratedAwayApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ApibootQuartzIntegratedAwayApplication.class, args);
    }

    /**
     * ApiBoot Quartz内置方法接口
     * {@link org.minbox.framework.api.boot.plugin.quartz.support.ApiBootQuartzServiceDefaultSupport}
     */
    @Autowired
    private ApiBootQuartzService quartzService;

    @Override
    public void run(String... args) throws Exception {
        quartzService.newJob(ApiBootOnceJobWrapper.Context()
                .jobClass(DemoJob.class)
                .wrapper());
    }
}
