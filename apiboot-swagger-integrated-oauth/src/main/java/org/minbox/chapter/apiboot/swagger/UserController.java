package org.minbox.chapter.apiboot.swagger;

import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 示例控制器
 *
 * @author 恒宇少年
 */
@RestController
@RequestMapping(value = "/user")
@Api(tags = "用户控制器")
public class UserController {
    /**
     * 示例：
     * 根据用户名查询用户基本信息
     *
     * @param name {@link UserResponse#getName()}
     * @return {@link UserResponse}
     */
    @GetMapping(value = "/{name}")
    @ApiOperation(value = "查询用户信息", response = UserResponse.class)
    @ApiResponse(code = 200, message = "success", response = UserResponse.class)
    public UserResponse getUser(@PathVariable("name") String name) {
        return new UserResponse(name, 25);
    }
    /**
     * 响应实体示例
     */
    @ApiModel
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class UserResponse {
        @ApiModelProperty(value = "用户名")
        private String name;
        @ApiModelProperty(value = "年龄")
        private Integer age;
    }
}
