package org.minbox.chapter.apiboot.swagger;

import org.minbox.framework.api.boot.autoconfigure.swagger.annotation.EnableApiBootSwagger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableApiBootSwagger
public class ApibootSwaggerIntegratedOauthApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApibootSwaggerIntegratedOauthApplication.class, args);
    }

}
