package org.minbox.chapter.apiboot.custom.logging.id;

import org.minbox.framework.api.boot.autoconfigure.logging.LoggingFactoryBeanCustomizer;
import org.minbox.framework.logging.client.LoggingFactoryBean;
import org.springframework.stereotype.Component;

/**
 * 自定义创建链路以及单元编号
 *
 * @author 恒宇少年
 * @see LoggingFactoryBeanCustomizer
 * @see LoggingFactoryBean
 * @see org.minbox.framework.logging.client.tracer.LoggingTraceGenerator
 * @see org.minbox.framework.logging.client.span.LoggingSpanGenerator
 */
@Component
public class CustomCreateTraceAndSpanId implements LoggingFactoryBeanCustomizer {
    /**
     * {@link CustomTraceIdGenerator} 自定义链路编号生成策略
     * {@link CustomSpanIdGenerator} 自定义单元编号生成策略
     *
     * @param factoryBean {@link LoggingFactoryBean}
     */
    @Override
    public void customize(LoggingFactoryBean factoryBean) {
        CustomTraceIdGenerator traceIdGenerator = new CustomTraceIdGenerator();
        factoryBean.setTraceGenerator(traceIdGenerator);

        CustomSpanIdGenerator spanIdGenerator = new CustomSpanIdGenerator();
        factoryBean.setSpanGenerator(spanIdGenerator);
    }
}
