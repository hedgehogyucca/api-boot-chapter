package org.minbox.chapter.apiboot.custom.logging.id;

import org.minbox.framework.logging.client.MinBoxLoggingException;
import org.minbox.framework.logging.client.span.LoggingSpanGenerator;

import java.util.UUID;

/**
 * 自定义单元编号生成策略
 *
 * @author 恒宇少年
 */
public class CustomSpanIdGenerator implements LoggingSpanGenerator {
    /**
     * 单元编号前缀
     */
    private static final String SPAN_ID_PREFIX = "group";

    @Override
    public String createSpanId() throws MinBoxLoggingException {
        return SPAN_ID_PREFIX + UUID.randomUUID().toString().hashCode();
    }
}
