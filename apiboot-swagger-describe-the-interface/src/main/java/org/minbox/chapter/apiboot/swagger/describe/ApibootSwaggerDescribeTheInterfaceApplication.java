package org.minbox.chapter.apiboot.swagger.describe;

import org.minbox.framework.api.boot.autoconfigure.swagger.annotation.EnableApiBootSwagger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableApiBootSwagger
public class ApibootSwaggerDescribeTheInterfaceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApibootSwaggerDescribeTheInterfaceApplication.class, args);
    }

}
