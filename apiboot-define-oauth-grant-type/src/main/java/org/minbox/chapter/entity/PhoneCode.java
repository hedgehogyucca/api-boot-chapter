package org.minbox.chapter.entity;

import com.gitee.hengboy.mybatis.enhance.common.annotation.Column;
import com.gitee.hengboy.mybatis.enhance.common.annotation.Id;
import com.gitee.hengboy.mybatis.enhance.common.annotation.Table;
import com.gitee.hengboy.mybatis.enhance.common.enums.KeyGeneratorTypeEnum;
import lombok.Data;

import java.sql.Timestamp;

/**
 * 手机号验证码信息表
 *
 * @author 恒宇少年
 */
@Data
@Table(name = "phone_code")
public class PhoneCode {
    /**
     * 验证码主键
     */
    @Column(name = "pc_id")
    @Id(generatorType = KeyGeneratorTypeEnum.AUTO)
    private Integer id;
    /**
     * 手机号
     */
    @Column(name = "pc_phone")
    private String phone;
    /**
     * 验证码内容
     */
    @Column(name = "pc_code")
    private String code;
    /**
     * 创建时间
     */
    @Column(name = "pc_create_time")
    private Timestamp createTime;
}
