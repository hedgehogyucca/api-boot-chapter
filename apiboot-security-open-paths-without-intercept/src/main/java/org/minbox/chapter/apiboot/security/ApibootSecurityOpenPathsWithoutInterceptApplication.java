package org.minbox.chapter.apiboot.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApibootSecurityOpenPathsWithoutInterceptApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApibootSecurityOpenPathsWithoutInterceptApplication.class, args);
    }

}
