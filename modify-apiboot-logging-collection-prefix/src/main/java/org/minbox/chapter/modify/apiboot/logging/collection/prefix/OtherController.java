package org.minbox.chapter.modify.apiboot.logging.collection.prefix;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 恒宇少年
 */
@RestController
public class OtherController {

    @GetMapping(value = "/other")
    public String other() {
        return "this is other path";
    }
}
