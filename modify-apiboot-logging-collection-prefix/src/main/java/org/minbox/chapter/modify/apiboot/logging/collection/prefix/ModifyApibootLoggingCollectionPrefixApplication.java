package org.minbox.chapter.modify.apiboot.logging.collection.prefix;

import org.minbox.framework.logging.spring.context.annotation.client.EnableLoggingClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 入口类
 *
 * @author 恒宇少年
 */
@SpringBootApplication
@EnableLoggingClient
public class ModifyApibootLoggingCollectionPrefixApplication {

    public static void main(String[] args) {
        SpringApplication.run(ModifyApibootLoggingCollectionPrefixApplication.class, args);
    }

}
