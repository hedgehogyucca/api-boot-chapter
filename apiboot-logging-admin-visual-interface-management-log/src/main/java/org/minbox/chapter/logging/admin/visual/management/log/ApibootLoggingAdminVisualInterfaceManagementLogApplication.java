package org.minbox.chapter.logging.admin.visual.management.log;

import org.minbox.framework.logging.spring.context.annotation.admin.EnableLoggingAdmin;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ApiBoot Logging Admin
 */
@SpringBootApplication
@EnableLoggingAdmin
public class ApibootLoggingAdminVisualInterfaceManagementLogApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApibootLoggingAdminVisualInterfaceManagementLogApplication.class, args);
    }

}
